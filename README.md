# Farcaster Research Project

The first research project on Farcaster was looking into vibes. The data for the analysis are interaction data (replies, mentions, recasts, and likes). All casts that received zero interactions were excluded. Closure, a social network metric was used to measure vibes.

Full blog post explaining the method and results:

Blog post going deeper into social network analysis: [Tiny Groups are the basis for innovations](https://netnigma.io/tiny-groups-are-the-basis-for-innovations/)

## Data

The analyzed channels were nouns, purple, farcasther, and data. The data was gathered using [Neynar](https://data.hubs.neynar.com/queries/316) by filtering for those channels. 

## Analysis

Run `create_network` to create the networks. Channel, frequency of casting, and interaction type (recast, like, mention, reply) are added as attributes.

Run `cross_net_report` for a quick cross-section report. This runs several network stats and produces a final graph. It can take some time for larger channels. 
- g: the graph generated from `create_networks`
- channel: what channel to focus on. Default is none.
- Etype: what interaction type to focus on. Default is none
- labels: if labels should be added to the network graph. Default is False. Making this true for large graphs is a bad idea. Currently labels are fids.
- nodedegree: Only draw casters (nodes) who had at least 2 interactions. Increase the limit for larger graphs. 

Run `time_networks` for a day-by-day anaysis.


The code in `channelgrowth.py` contains several try-and-failed analytical avenus, and graphs that didn't make it into the final article. These are left in but commented. 

## What about channel ...?

Feel free to run the analysis for whatever channel you want, or reach out to me on Farcaster ([kbc](https://warpcast.com/kbc)) for help. 
