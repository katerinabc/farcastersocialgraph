#!/usr/bin/env python3 

import logging
import os
import sys

# import requests
# import time
# import json

from datetime import date
import networkx as nx
import pandas as pd
import matplotlib.pyplot as plt


def create_network(data):
    
    # clean dataset: remove self-loop
    df = data[data['to'] != data['from']]
    
    # create network
    G = nx.from_pandas_edgelist(df, source = 'from', target = 'to', edge_attr=['count', 'Etype', 'channelname'], create_using=nx.DiGraph)

    # add fids
    fid_labels = dict(zip(G.nodes, G.nodes))
    nx.set_node_attributes(G, fid_labels, 'fid')

    return(G)

def gsubset(g, channel = None, Etype = None):

    if (channel is None) and (Etype is None):
        # print(channel is None and Etype is None)
        graph = g

    # subset for channel
    elif (channel is not None) and (Etype is None):
            # channel is not None and Etype is None:
            # print(channel is not None and Etype is None)
            graph = nx.DiGraph(((source, target, attr) for source, target, attr in g.edges(data=True) if attr['channelname'] == channel))

    # subet for type
    elif (channel is None) and (Etype is not None):
        graph = nx.DiGraph(((source, target, attr) for source, target, attr in g.edges(data=True) if attr['Etype'] == Etype))
        
    else:
        graph1 = nx.DiGraph(((source, target, attr) for source, target, attr in g.edges(data=True) if attr['channelname'] == channel))
        graph2 = nx.DiGraph(((source, target, attr) for source, target, attr in g.edges(data=True) if attr['Etype'] == Etype))
        graph = nx.intersection(graph1, graph2)
    
    return(graph)

def cross_net_report(g, channel = None, Etype = None, labels = False, nodedegree = 2):

    if (channel is None) and (Etype is None):
        graph = g

    # subset for channel
    elif (channel is not None) and (Etype is None):
        graph = nx.DiGraph(((source, target, attr) for source, target, attr in g.edges(data=True) if attr['channelname'] == channel))

            
     # subet for type
    elif (channel is None) and (Etype is not None):
        graph = nx.DiGraph(((source, target, attr) for source, target, attr in g.edges(data=True) if attr['Etype'] == Etype))
        
    else:
        graph1 = nx.DiGraph(((source, target, attr) for source, target, attr in g.edges(data=True) if attr['channelname'] == channel))
        graph2 = nx.DiGraph(((source, target, attr) for source, target, attr in g.edges(data=True) if attr['Etype'] == Etype))
        graph = nx.intersection(graph1, graph2)


    # basic descriptives

    nbr_nodes = nx.number_of_nodes(graph)
    nbr_edges = nx.number_of_edges(graph)
    nbr_den = nx.density(graph)
    try:
        avg_clustering = nx.average_clustering(graph)
    except:
        pass

    # node level stats

    popularity = nx.centrality.in_degree_centrality(graph)
    activity = nx.centrality.out_degree_centrality(graph)
    clustcoef = nx.clustering(graph)

    nx.set_node_attributes(graph, popularity, "Popularity")
    nx.set_node_attributes(graph, activity, "Popularity")
    nx.set_node_attributes(graph, clustcoef, "ClusteringCoeff")


    # tribes
    fccom = nx.community.louvain_communities(graph)

    for count, cluster in enumerate(fccom, 1):
        tmp = [x for x in cluster]
        #create a dictionary of cluster id and fid
        nx.set_node_attributes(graph, pd.Series(count, tmp).to_dict(), 'Tribe')

    # reciprocity

    g_recip = nx.overall_reciprocity(graph)

    # triad census - takes time
    
    triads = nx.triadic_census(graph)

    # plot graph

    graph.remove_nodes_from(node for node, degree in dict(graph.degree()).items() if degree < nodedegree)

    fig, ax = plt.subplots(figsize=(10, 10))
    pos = nx.spring_layout(graph, k=0.15, seed=4572321)
    node_color = list(nx.get_node_attributes(graph, 'Tribe').values())
    node_size = [v* 2000 for v in list(nx.get_node_attributes(graph, 'ClusteringCoeff').values())]
    edge_weight = list(nx.get_edge_attributes(graph, 'count').values())
    
    # if channel == None & type == None:
    #     edge_color = list(nx.get_edge_attributes(graph, 'channelname').values())
    #     if channel == None & type != None:
    #         edge_color = list(nx.get_edge_attributes(graph, 'type').values())
    #         if channel != None & type == None:
    #             edge_color = list(nx.get_edge_attributes(graph, 'channelname').values())
    #         else:
    #             edge_color = list(nx.get_edge_attributes(graph, 'channelname').values())
    
    nx.draw_networkx(
    graph,
    pos=pos,
    node_color=node_color,
    with_labels=labels,
    # edge_color=edge_color,
    width = edge_weight,
    alpha=0.4,
    )

    # Title/legend
    font = {"color": "k", "fontweight": "bold", "fontsize": 20}
    ax.set_title("Social Graph of Channel {channel} and type {type} (color = Tribe)".format(channel=channel, type = type), font)

    # Resize figure for label readability
    ax.margins(0.1, 0.05)
    fig.tight_layout()
    plt.axis("off")


    # print stats

    # print them to file
    print('Stats for channel {channel} and type {type}'.format(channel=channel, type = type))
    print('nbr nodes: ', nbr_nodes)
    print('nbr edges: ', nbr_edges)
    print('density: ', round(nbr_den,4))
    print('Number of Tribes: ', len(fccom))
    print('Proportion of Reciprocal edges: ', round(g_recip, 4))
    print('Average clustering coefficient: ', round(avg_clustering,4))


    return([graph, triads, plt])

def time_networks(df, channel = None, Etype = None):
    df = df[df['to'] != df['from']]

    tmp = df[['from', 'to', 'Etype', 'channelname', 'date', 'timestamp']].groupby(['from', 'to', 'channelname', 'Etype', 'date'], as_index=False).count()
    tmp = tmp.rename(columns = {'timestamp': 'count'})
    # create network
    g = nx.from_pandas_edgelist(tmp, source = 'from', target = 'to', edge_attr=['count', 'Etype', 'channelname', 'date'], create_using=nx.DiGraph)

    # add fids
    fid_labels = dict(zip(g.nodes, g.nodes))
    nx.set_node_attributes(g, fid_labels, 'fid')

    # create subsect for analysis
    # subset here for time?

    # filter by channel or type
    if (channel is None) and (Etype is None):
        graph0 = g

    # subset for channel
    elif (channel is not None) and (Etype is None):
        graph0 = nx.DiGraph(((source, target, attr) for source, target, attr in g.edges(data=True) if attr['channelname'] == channel))

            
     # subet for type
    elif (channel is None) and (Etype is not None):
        graph0 = nx.DiGraph(((source, target, attr) for source, target, attr in g.edges(data=True) if attr['Etype'] == Etype))
        
    else:
        graph1 = nx.DiGraph(((source, target, attr) for source, target, attr in g.edges(data=True) if attr['channelname'] == channel))
        graph2 = nx.DiGraph(((source, target, attr) for source, target, attr in g.edges(data=True) if attr['Etype'] == Etype))
        graph0 = graph1.copy()
        graph0.remove_nodes_from(n for n in graph1 if n not in graph2)
        graph0.remove_edges_from(e for e in graph1.edges if e not in graph2.edges)
        # graph0 = nx.intersection(graph1, graph2)

    # create time slice
    # gdates = df.date.drop_duplicates()
    gdates = set([attr for source, target, attr in graph0.edges(data='date')])

    dict_time = {}
    for castdate in gdates:
        print(castdate)
        try: 
            graph = nx.DiGraph(((source, target, attr) for source, target, attr in graph0.edges(data=True) if attr['date'] == castdate))
        
            # calculate graph metrics
            # basic descriptives

            tmp_nodes = nx.number_of_nodes(graph)
            tmp_edges = nx.number_of_edges(graph)
            tmp_den = nx.density(graph)
            try:
                tmp_avg_clustering = nx.average_clustering(graph)
                g_recip = nx.overall_reciprocity(graph)
                triads = nx.triadic_census(graph)
            except:
                tmp_avg_clustering = 0
                # pass
            if triads.get('021C') == 0:
                clVSOp = ((1 + triads.get('030T'))/ (1 + triads.get('021C')) -1 )
            else:
                clVSOp = triads.get('030T')/ triads.get('021C')


            # save graph metrics into dictionary
            dict_time[castdate] = {'channel': channel, 
                            'Etype': Etype, 
                            'nbr_nodes': tmp_nodes, 
                            'nbr_edges': tmp_edges, 
                            'nbr_den': tmp_den, 
                            'nbr_clustering': tmp_avg_clustering, 
                            'reciprocity': g_recip,
                            'closure': triads.get('030T'),
                            'openTriangle': triads.get('021U'),
                            'chain': triads.get('021C'),
                            'cycle': triads.get('030C'),
                            'completeTriangle': triads.get('300'),
                            'closedVSOpen': clVSOp
                            }
            print('ok =', castdate, 'network has', nx.number_of_edges(graph), 'edges.' )
        except:
            print('error occured at date =', castdate, 'network has', nx.number_of_edges(graph), 'edges.' )
            # pass

    
    ts_df = pd.DataFrame.from_dict(dict_time, 'index')
    ts_df = ts_df.reset_index()
    
    return([ts_df, graph0])

def plot_time_networs(df, channel = None, Etype = None, labels = False, nodedegree = 2):
    df = df[df['to'] != df['from']]

    tmp = df[['from', 'to', 'Etype', 'channelname', 'date', 'timestamp']].groupby(['from', 'to', 'channelname', 'Etype', 'date'], as_index=False).count()
    tmp = tmp.rename(columns = {'timestamp': 'count'})
    # create network
    g = nx.from_pandas_edgelist(tmp, source = 'from', target = 'to', edge_attr=['count', 'Etype', 'channelname', 'date'], create_using=nx.DiGraph)

    # add fids
    fid_labels = dict(zip(g.nodes, g.nodes))
    nx.set_node_attributes(g, fid_labels, 'fid')

    # create subsect for analysis
    # subset here for time?

    # filter by channel or type
    if (channel is None) and (Etype is None):
        graph0 = g

    # subset for channel
    elif (channel is not None) and (Etype is None):
        graph0 = nx.DiGraph(((source, target, attr) for source, target, attr in g.edges(data=True) if attr['channelname'] == channel))

            
     # subet for type
    elif (channel is None) and (Etype is not None):
        graph0 = nx.DiGraph(((source, target, attr) for source, target, attr in g.edges(data=True) if attr['Etype'] == Etype))
        
    else:
        graph1 = nx.DiGraph(((source, target, attr) for source, target, attr in g.edges(data=True) if attr['channelname'] == channel))
        graph2 = nx.DiGraph(((source, target, attr) for source, target, attr in g.edges(data=True) if attr['Etype'] == Etype))
        graph0 = nx.intersection(graph1, graph2)

    # create time slice
    gdates = df.date.drop_duplicates()

    i = 1
    for date in gdates:
        #print(i)
        #print(len(gdates))
        print("Progress: ", round(i/len(gdates),2))
        graph = nx.DiGraph(((source, target, attr) for source, target, attr in graph0.edges(data=True) if attr['date'] == date))

        graph.remove_nodes_from(node for node, degree in dict(graph.degree()).items() if degree < nodedegree)

        fig, ax = plt.subplots(figsize=(10, 10))
        pos = nx.spring_layout(graph, k=0.15, seed=4572321)
        #node_color = list(nx.get_node_attributes(graph, 'Tribe').values())
        #node_size = [v* 2000 for v in list(nx.get_node_attributes(graph, 'ClusteringCoeff').values())]
        edge_weight = list(nx.get_edge_attributes(graph, 'count').values())
        
        nx.draw_networkx(
        graph,
        pos=pos,
        #node_color=node_color,
        with_labels=labels,
        # edge_color=edge_color,
        width = edge_weight,
        alpha=0.4,
        )

        # Title/legend
        # font = {"color": "k", "fontweight": "bold", "fontsize": 20}
        # ax.set_title("Social Graph of Channel {channel} and type {type} (color = Tribe)".format(channel=channel, type = type), font)

        # Resize figure for label readability
        ax.margins(0.1, 0.05)
        fig.tight_layout()
        plt.axis("off")

        plt.savefig('graph_{channel}_{type}_{date}'.format(channel = channel, type = type, date = date), bbox_inches='tight')
        plt.close()
        
        i= i+1

 

def createLabels(data):
    df = pd.DataFrame(data)
    senders = df[['sender', 'fnsender']].drop_duplicates().rename(columns={'sender':'fid', 'fnsender': 'fname'})
    receivers = df[['receiver', 'fnreceiver']].drop_duplicates().rename(columns={'receiver':'fid', 'fnreceiver': 'fname'})
    nodes = pd.concat([senders, receivers]).drop_duplicates()
    labels = dict(zip(nodes.fid, nodes.fname))

    return(labels)

def assign_attributes(g, data):
    # add node labels
    # unique senders
    senders = data[['sender', 'fnsender']].drop_duplicates().rename(columns={'sender':'fid', 'fnsender': 'fname'})
    receivers = data[['receiver', 'fnreceiver']].drop_duplicates().rename(columns={'receiver':'fid', 'fnreceiver': 'fname'})
    nodes = pd.concat([senders, receivers]).drop_duplicates()
    labels = dict(zip(nodes.fid, nodes.fname))
    nx.set_node_attributes(g, labels, 'fname' )

    # add fids
    fid_labels = dict(zip(g.nodes, g.nodes))
    nx.set_node_attributes(g, fid_labels, 'fid')

    return g
