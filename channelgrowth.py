#!/usr/bin/env python3 

# What makes channels pick up and stick?

# RQ: What channel is more cohesive? 
# RQ: Does it matter if the community moved from one place to another?
# RQ: What brings a community together? Recasts or replie
import networkx as nx
import pandas as pd
import sys
import os
# script_dir = os.path.dirname( __file__ )
# helpers_dir = os.path.join(script_dir, '..',  'helpers')
# sys.path.append('/helpers')
# set working directory if necessary
import researchFC.researchhelpers as hlp
#import researchhelpers as hlp
import matplotlib.pyplot as plt
import seaborn as sns
sns.set_theme()

# the data has been pulled in using Naynar

#### load data
ldf = pd.read_csv("inputData/Specific_channel_research_-_reactions_2024_02_10.csv")
hdf = pd.read_csv("inputData/Specific_channel_research_-_casts_2024_02_10.csv")

# reaction_type
# 1 = like
# 2 = recast

# remove square brackets in string in mentions
hdf['mentions'] = hdf.mentions.map(lambda x: x.lstrip('[').rstrip(']'))

#unpack mentions column
hdf = hdf.assign(mentions = hdf.mentions.str.split(',')).explode('mentions')


#### create network: low-interaction

ldf = ldf.rename(columns={'fid': 'from', 'target_fid':'to'})
re1df = ldf[ldf['reaction_type'] == 1][['from', 'to', 'channelname', 'timestamp']]
re1df['Etype'] = 'like'
re2df = ldf[ldf['reaction_type'] == 2][['from', 'to', 'channelname', 'timestamp']]
re2df['Etype'] = 'recast'

#### create network: high-interaction

repdf = hdf[['fid', 'parent_fid', 'channelname', 'timestamp', ]].drop_duplicates()
repdf['Etype'] = 'reply'
repdf = repdf.rename(columns = {'fid': 'from', 'parent_fid': 'to'})
mendf = hdf[['fid', 'mentions', 'channelname', 'timestamp', ]].drop_duplicates()
mendf['Etype'] = 'mention'
mendf = mendf.rename(columns = {'fid': 'from', 'mentions': 'to'})

#### put it all together again

df = pd.concat([re1df, re2df, repdf, mendf],ignore_index=True)

### transform date
df['date'] = pd.to_datetime(df['timestamp'], format = "%d/%m/%y %H:%M")
df['date'] = df['date'].dt.date
# # extract the day, month, and year components
# df['day'] = df['date'].dt.day
# df['month'] = df['date'].dt.month
# df['year'] = df['date'].dt.year

### categorize etype into high (reply + mention) and low (likes + recast)
df['Etype_raw'] = df['Etype']

# df['Etype'] = [2 for x if x == 'reply' or x == 'mention' else 1 for x in df['Etype']]
df['Etype'] = ['high' if x == 'reply' or x == 'mention' else 'low' for x in df['Etype']] 

# remove timestamp and count
crossdf = df.groupby(['from', 'to', 'Etype', 'channelname'], as_index=False).count()
crossdf = crossdf.rename(columns = {'timestamp': 'count'})

#### Overall channel activity - Cross section

g = hlp.create_network(crossdf)

chdata = hlp.cross_net_report(g, channel = 'data', Etype = None, labels = False, nodedegree = 2)
chnouns = hlp.cross_net_report(g, channel = 'nouns', Etype = None, labels = False, nodedegree = 2)
chfarcaster = hlp.cross_net_report(g, channel = 'FarcastHer', Etype = None, labels = False, nodedegree = 2)
chpurple = hlp.cross_net_report(g, channel = 'purple', Etype = None, labels = False, nodedegree = 2)

# #### Crude longitudinal analysis

# # Just exploring a bit... uncomment if you want to see the results

# tsdf = df[['channelname', 'timestamp', 'Etype', 'date']].groupby(['channelname', 'Etype', 'date'], as_index=False).count()
# tsdf = tsdf.rename(columns = {'timestamp': 'count'})

# sns.lineplot(x="date", y="count",
#              hue="channelname", style="Etype",
#              data=tsdf)

# # remove nouns. More active than other channels

# tsdf = tsdf[tsdf['channelname'] != 'nouns']
# sns.lineplot(x="date", y="count",
#              hue="channelname", style="Etype",
#              data=tsdf)

# # ok, having it by channel and type creates too much noise. add nouns back and count by channel, ignoring type

# tsdf = df[['channelname', 'timestamp','date']].groupby(['channelname', 'date'], as_index=False).count()
# tsdf = tsdf.rename(columns = {'timestamp': 'count'})

# sns.lineplot(x="date", y="count",
#              hue="channelname", 
#              data=tsdf)

# # again removing nouns

# tsdf = tsdf[tsdf['channelname'] != 'nouns']
# sns.lineplot(x="date", y="count",
#              hue="channelname",
#              data=tsdf)


# Create a dataset per channel

chts1 = hlp.time_networks(df, channel = 'data', Etype = None)
chts2 = hlp.time_networks(df, channel = 'nouns', Etype = None)
chts3 = hlp.time_networks(df, channel = 'purple', Etype = None)
chts4 = hlp.time_networks(df, channel = 'FarcastHer', Etype = None)

channel_ts = pd.concat([chts1[0], chts2[0], chts3[0], chts4[0]])
channel_ts.sort_values('index')

sns.violinplot(x = 'channel', y = 'nbr_nodes', data = channel_ts)
sns.violinplot(x = 'channel', y = 'nbr_edges', data = channel_ts)



### RQ: Reciprocity

# sns.violinplot(x = 'channel', y = 'reciprocity', data = channel_ts) # not included

# plot 1: facet graph
plot = sns.relplot( x = 'index', 
             y = 'reciprocity', 
             col = 'channel',
             hue = 'channel', 
             data = channel_ts,
             legend = False,
             kind = 'line')

# # plot 2: line graph
# plot = sns.lineplot( x = 'index', 
#              y = 'reciprocity', 
#              hue = 'channel', 
#              data = channel_ts)

# # Customize x-axis tick labels
# xtick_loc = plot.get_xticks()
# xtick_labels = plot.get_xticklabels()
# plot.set_xticks(ticks = xtick_loc, labels = xtick_labels, rotation = 45, ha = 'right', fontsize = 8)

# # Specfiy axis labels and title
# plot.set(xlabel = None, ylabel = 'Reciprocity', title = 'Evolution of Reciprocity')
# plt.show()

# # plot 3 scatter graph
# plot = sns.count( x = 'index', 
#              y = 'reciprocity', 
#              col = 'channel',
#              hue = 'channel', 
#              data = channel_ts)

# # Customize x-axis tick labels
# xtick_loc = plot.get_xticks()
# xtick_labels = plot.get_xticklabels()
# plot.set_xticks(ticks = xtick_loc, labels = xtick_labels, rotation = 45, ha = 'right', fontsize = 8)

# # Specfiy axis labels and title
# plot.set(xlabel = None, ylabel = 'Reciprocity', title = 'Evolution of Reciprocity')
# plt.show()


### RQ: does the clustering coefficient increase over time in the channels?

plot = sns.relplot( x = 'index', 
             y = 'nbr_clustering', 
             col = 'channel',
             hue = 'channel', 
             data = channel_ts,
             legend = False,
             kind = 'line')


# process: network for d1 for channel 1 (ignoring type). calcualte avg_clustering

# sns.violinplot(x = 'channel', y = 'nbr_clustering', data = channel_ts)

# sns.violinplot(x = 'channel', y = 'closure', data = channel_ts)
# useless graph because of nouns

# plot: line graph of closure for each channel over time. hard to see pattern
plot = sns.lineplot( x = 'index', 
             y = 'closure', 
             hue = 'channel', 
             data = channel_ts)

# Customize x-axis tick labels
xtick_loc = plot.get_xticks()
xtick_labels = plot.get_xticklabels()
plot.set_xticks(ticks = xtick_loc, labels = xtick_labels, rotation = 45, ha = 'right', fontsize = 8)

# Specfiy axis labels and title
plot.set(xlabel = None, ylabel = 'Number of 030T triangles', title = 'Evolution of Closure')
plt.show()

# plot number of triangles without nouns
plot = sns.lineplot( x = 'index', 
             y = 'closure', 
             hue = 'channel', 
             data = channel_ts[channel_ts['channel'] != 'nouns'])

# Customize x-axis tick labels
xtick_loc = plot.get_xticks()
xtick_labels = plot.get_xticklabels()
plot.set_xticks(ticks = xtick_loc, labels = xtick_labels, rotation = 45, ha = 'right', fontsize = 8)

# Specfiy axis labels and title
plot.set(xlabel = None, ylabel = 'Number of 030T triangles', title = 'Evolution of Closure')
plt.show()

# digging deeper into closure (triangles)

# proportion of open (030T) vs closed triangles (021U)
# the diff closed vs open triangle is huge. not gonna plot them into one graph

# channelList = [chts1, chts2, chts3, chts4]
# for channel in channelList:
#     channel['ClosedVsOpen'] = channel['closure']/channel['openTriangle']

# channel_ts['ClosedVsOpen'] = channel_ts['closure']/channel_ts['openTriangle']

channel_ts
sns.lineplot(x = 'index', y = 'openTriangle', hue = 'channel', data = channel_ts)
sns.lineplot(x = 'index', y = 'closedVSOpen', hue = 'channel', data = channel_ts[channel_ts['closedVSOpen'].isna() == False])


# Customize x-axis tick labels
xtick_loc = plot.get_xticks()
xtick_labels = plot.get_xticklabels()
plot.set_xticks(ticks = xtick_loc, labels = xtick_labels, rotation = 45, ha = 'right', fontsize = 8)

sns.relplot(x = 'index', y = 'closedVSOpen', 
            col = 'channel', hue = 'channel', 
            data = channel_ts[channel_ts['closedVSOpen'].isna() == False])


# Specfiy axis labels and title
plot.set(xlabel = None, ylabel = 'Number of 030T triangles', title = 'Evolution of Closure')
plt.show()
# interesting. There are some spikes across all channels (Ecosystem effects)
# but also some channel specific movements, notably in FarcastHer 
# that aren't happening in the other channels
# interesting is also that nouns isn't breaking the channel

sns.relplot(x = 'index', y = 'closedVSOpen', col = 'channel', kind = 'line',
             hue = 'channel', data = channel_ts[channel_ts['closedVSOpen'].isna() == False])

# looking for patterns in those other triad measures
sns.relplot(x = 'index', y = 'chain', col = 'channel', hue = 'channel', data = channel_ts)
sns.relplot(x = 'index', y = 'cycle', col = 'channel', hue = 'channel', data = channel_ts)
sns.relplot(x = 'index', y = 'completeTriangle', col = 'channel', hue = 'channel', data = channel_ts)




# Only looking at high level interaction (mention and replies)
# not used for article

# chts1_2 = hlp.time_networks(df, channel = 'data', Etype = 'high')
# sns.relplot(x = 'index', y = 'reciprocity', col = 'Etype', hue = 'Etype', data = chts1_2[0])

# chts2_2 = hlp.time_networks(df, channel = 'nouns', Etype = 'high')
# sns.relplot(x = 'index', y = 'reciprocity', col = 'Etype', hue = 'Etype', data = chts2_2[0])

# chts3_2 = hlp.time_networks(df, channel = 'purple', Etype = 'high')
# sns.relplot(x = 'index', y = 'reciprocity', col = 'Etype', hue = 'Etype', data = chts3_2[0])

# chts4_2 = hlp.time_networks(df, channel = 'FarcastHer', Etype = 'high')
# sns.relplot(x = 'index', y = 'reciprocity', col = 'Etype', hue = 'Etype', data = chts4_2[0])




### plot time networks

# This greates one graph for each time date. Run at your own risk

# hlp.plot_time_networs(df, 'data', nodedegree=6)

#################################################
####### Trial and error graphs

# focusing on data channel

tsdf = df[['channelname', 'timestamp', 'Etype', 'date']].groupby(['channelname', 'Etype', 'date'], as_index=False).count()
tsdf = tsdf.rename(columns = {'timestamp': 'count'})
tsdf = tsdf[tsdf['channelname'] == 'data']
sns.lineplot(x="date", y="count",
             hue="Etype",
             data=tsdf)


sns.scatterplot( x = 'index', 
             y = 'nbr_nodes', 
             hue = 'channel', 
             data = channel_ts)

sns.scatterplot( x = 'index', 
             y = 'nbr_edges', 
             hue = 'channel', 
             data = channel_ts)

plot = sns.relplot( x = 'index', 
             y = 'closure', 
             col = 'channel',
             hue = 'channel', 
             data = channel_ts,
             legend = False,
             kind = 'line')

plot = sns.lineplot( x = 'index', 
             y = 'closure', 
             hue = 'channel', 
             data = channel_ts)
